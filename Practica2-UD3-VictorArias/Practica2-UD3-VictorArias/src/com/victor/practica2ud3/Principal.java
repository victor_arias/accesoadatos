package com.victor.practica2ud3;

import com.victor.practica2ud3.gui.Controlador;
import com.victor.practica2ud3.gui.Modelo;
import com.victor.practica2ud3.gui.Vista;


public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}