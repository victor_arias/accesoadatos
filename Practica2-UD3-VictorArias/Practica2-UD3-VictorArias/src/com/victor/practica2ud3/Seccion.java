package com.victor.practica2ud3;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "secciones", schema = "supermercado", catalog = "")
public class Seccion {
    private int id;
    private String nombre;
    private String tamanyo;
    private boolean abierta;
    private List<SeccionProducto> seccionproducto;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "tamanyo")
    public String getTamanyo() {
        return tamanyo;
    }

    public void setTamanyo(String tamanyo) {
        this.tamanyo = tamanyo;
    }

    @Basic
    @Column(name = "abierta")
    public boolean isAbierta() {
        return abierta;
    }

    public void setAbierta(boolean abierta) {
        this.abierta = abierta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seccion seccion = (Seccion) o;
        return id == seccion.id &&
                abierta == seccion.abierta &&
                Objects.equals(nombre, seccion.nombre) &&
                Objects.equals(tamanyo, seccion.tamanyo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, tamanyo, abierta);
    }

    @OneToMany(mappedBy = "seccion")
    public List<SeccionProducto> getSeccionproducto() {
        return seccionproducto;
    }

    public void setSeccionproducto(List<SeccionProducto> seccionproducto) {
        this.seccionproducto = seccionproducto;
    }

    @Override
    public String toString() {
        return id + "-" + nombre;
    }
}
