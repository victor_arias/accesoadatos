package com.victor.practica2ud3.gui;

import javax.swing.*;
import java.awt.*;

public class OptionDialog extends JDialog {


    private JPanel panel1;
    public JButton btnOpcionesGuardar;
    JTextField tfUser;
    JPasswordField pfPass;
    private Frame owner;

    /**
     * Constructor de la clase.
     * @param owner propietario del dialog
     */
    public OptionDialog(Frame owner) {
        super(owner, "Opciones", true);
        this.owner = owner;
        initDialog();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(owner);
    }
}

