package com.victor.practica2ud3.gui;


import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{
    private JFrame frame;
    private JPanel panel1;
    JTabbedPane tabbedPane1;
    JTextField txtIdS;
    JTextField txtNombreS;
    JTextField txtTanayoS;
    JButton altaSeccionButton;
    JButton modificarSeccionButton;
    JButton listarSeccionesButton;
    JButton borrarSeccionButton;
    JTextField txtIdM;
    JTextField txtNombreM;
    JTextField txtOrigenM;
    JTextField txtPaisM;
    JButton altaMarcaButton;
    JButton modificarMarcaButton;
    JButton listarMarcasButton;
    JButton borrarMarcaButton;
    JTextField txtIdE;
    JTextField txtNombreE;
    JTextField txtEmailE;
    JTextField txtTelefonoE;
    JTextField txtTipoEmpresaE;
    JTextField txtWebE;
    JButton altaEmpresaButton;
    JButton modificarEmpresaButton;
    JButton listarEmpresasButton;
    JButton borrarEmpresaButton;
    JList listEmpresas;
    JTextField txtNombreP;
    JTextField txtIdP;
    JTextField txtCodigoP;
    JTextField txtPrecioP;
    JButton altaProductoButton;
    JButton modificarProductoButton;
    JButton listarProductosButton;
    JButton borrarProductoButton;
    JList listProductos;
    com.github.lgooddatepicker.components.DatePicker fechaCaducidadP;
    DatePicker fechaCreacionM;
    JList listProductosEmpresa;
    JList listMarcas;
    JList listSecciones;
    JList listProductosMarca;
    JComboBox comboBoxMarcaP;
    JComboBox comboBoxEmpresaP;
    JCheckBox checkBoxAbiertaS;

    DefaultListModel dlmProductos;
    DefaultListModel dlmEmpresas;
    DefaultListModel dlmSecciones;
    DefaultListModel dlmMarcas;
    DefaultListModel dlmProductosEmpresa;
    DefaultListModel dlmProductosMarca;

    JMenuItem conexionItem;
    JMenuItem itemOpciones;
    JMenuItem salirItem;

    /*OPTION DIALOG*/
    OptionDialog optionDialog;

    public Vista(){
        frame = new JFrame("Vista");
        frame.setBounds(300,200,800,600);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+300, this.getHeight()+200));
        frame.setLocationRelativeTo(null);

        this.optionDialog = new OptionDialog(this);

        crearModelos();
        crearMenu();
    }

    private void crearModelos() {
        dlmProductos = new DefaultListModel();
        listProductos.setModel(dlmProductos);

        dlmEmpresas = new DefaultListModel();
        listEmpresas.setModel(dlmEmpresas);

        dlmMarcas = new DefaultListModel();
        listMarcas.setModel(dlmMarcas);

        dlmSecciones = new DefaultListModel();
        listSecciones.setModel(dlmSecciones);

        dlmProductosEmpresa = new DefaultListModel();
        listProductosEmpresa.setModel(dlmProductosEmpresa);

        dlmProductosMarca = new DefaultListModel();
        listProductosMarca.setModel(dlmProductosMarca);
    }

    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        itemOpciones = new JMenuItem("OpcionesConexion");
        itemOpciones.setActionCommand("OpcionesConexion");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(itemOpciones);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}
