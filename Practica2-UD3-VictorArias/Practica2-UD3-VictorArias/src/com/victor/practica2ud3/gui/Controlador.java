package com.victor.practica2ud3.gui;

import com.victor.practica2ud3.Empresa;
import com.victor.practica2ud3.Marca;
import com.victor.practica2ud3.Producto;
import com.victor.practica2ud3.Seccion;
import org.hibernate.cfg.Environment;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, ItemListener, WindowListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addListSelectionListener(this);
        addActionListeners(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                //vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "OpcionesConexion":
               vista.optionDialog.setVisible(true);
                break;

            case "GuardarTemporalmente":
                modelo.user = vista.optionDialog.tfUser.getText();
                modelo.pass = String.valueOf(vista.optionDialog.pfPass.getPassword());
                vista.optionDialog.dispose();
                modelo.conectar();
                break;

            /*Producto*/
            case "Alta Producto":
                Producto nuevoProducto = new  Producto();
                nuevoProducto.setMarca((Marca) vista.comboBoxMarcaP.getSelectedItem());
                nuevoProducto.setEmpresa((Empresa) vista.comboBoxEmpresaP.getSelectedItem());
                nuevoProducto.setNombre(vista.txtNombreP.getText());
                nuevoProducto.setCodigo(vista.txtCodigoP.getText());
                nuevoProducto.setPrecio(Double.parseDouble(vista.txtPrecioP.getText()));
                nuevoProducto.setFechacaducidad(Date.valueOf(vista.fechaCaducidadP.getDate()));
                modelo.altaProducto(nuevoProducto);
                borrarCamposProductos();
                break;

            case "Listar Productos":
                listarProductos(modelo.getProductos());
                break;

            case "Modificar Producto":
                Producto productoSeleccion = (Producto)vista.listProductos.getSelectedValue();
                productoSeleccion.setMarca((Marca) vista.comboBoxMarcaP.getSelectedItem());
                productoSeleccion.setEmpresa((Empresa) vista.comboBoxEmpresaP.getSelectedItem());
                productoSeleccion.setNombre(vista.txtNombreP.getText());
                productoSeleccion.setCodigo(vista.txtCodigoP.getText());
                productoSeleccion.setPrecio(Double.parseDouble(vista.txtPrecioP.getText()));
                productoSeleccion.setFechacaducidad(Date.valueOf(vista.fechaCaducidadP.getDate()));
                modelo.modificarProducto(productoSeleccion);
                borrarCamposProductos();
                break;

            case "Borrar Producto":
                Producto productoBorrado  = (Producto)vista.listProductos.getSelectedValue();
                modelo.borrarProducto(productoBorrado);
                listarProductos(modelo.getProductos());
                break;

            /*Empresa*/
            case "Alta Empresa":
                Empresa nuevaEmpresa = new  Empresa();
                nuevaEmpresa.setNombre(vista.txtNombreE.getText());
                nuevaEmpresa.setEmail(vista.txtEmailE.getText());
                nuevaEmpresa.setTelefono(vista.txtTelefonoE.getText());
                nuevaEmpresa.setTipoempresa(vista.txtTipoEmpresaE.getText());
                nuevaEmpresa.setWeb(vista.txtWebE.getText());
                modelo.altaEmpresa(nuevaEmpresa);
                borrarCamposEmpresas();
                break;

            case "Listar Empresas":
                listarEmpresas(modelo.getEmpresas());
                break;

            case "Modificar Empresa":
                Empresa empresaSeleccion = (Empresa)vista.listEmpresas.getSelectedValue();
                empresaSeleccion.setNombre(vista.txtNombreE.getText());
                empresaSeleccion.setEmail(vista.txtEmailE.getText());
                empresaSeleccion.setTelefono(vista.txtTelefonoE.getText());
                empresaSeleccion.setTipoempresa(vista.txtTipoEmpresaE.getText());
                empresaSeleccion.setWeb(vista.txtWebE.getText());
                modelo.modificarEmpresa(empresaSeleccion);
                borrarCamposEmpresas();
                break;

            case "Borrar Empresa":
                Empresa empresaBorrada  = (Empresa)vista.listEmpresas.getSelectedValue();
                modelo.borrarEmpresa(empresaBorrada);
                listarEmpresas(modelo.getEmpresas());
                break;

            /*Marca*/
            case "Alta Marca":
                Marca nuevaMarca = new  Marca();
                nuevaMarca.setNombre(vista.txtNombreM.getText());
                nuevaMarca.setOrigen(vista.txtOrigenM.getText());
                nuevaMarca.setFechacreacion(Date.valueOf(vista.fechaCreacionM.getDate()));
                nuevaMarca.setPais(vista.txtPaisM.getText());
                modelo.altaMarca(nuevaMarca);
                borrarCamposMarcas();
                break;

            case "Listar Marcas":
                listarMarcas(modelo.getMarcas());
                break;

            case "Modificar Marca":
                Marca marcaSeleccion = (Marca)vista.listMarcas.getSelectedValue();
                marcaSeleccion.setNombre(vista.txtNombreM.getText());
                marcaSeleccion.setOrigen(vista.txtOrigenM.getText());
                marcaSeleccion.setFechacreacion(Date.valueOf(vista.fechaCreacionM.getDate()));
                marcaSeleccion.setPais(vista.txtPaisM.getText());
                modelo.modificarMarca(marcaSeleccion);
                borrarCamposMarcas();
                break;

            case "Borrar Marca":
                Marca marcaBorrada  = (Marca)vista.listMarcas.getSelectedValue();
                modelo.borrarMarca(marcaBorrada);
                listarMarcas(modelo.getMarcas());
                break;


            /*Seccion*/
            case "Alta Seccion":
                Seccion nuevaSeccion = new  Seccion();
                nuevaSeccion.setNombre(vista.txtNombreS.getText());
                nuevaSeccion.setTamanyo(vista.txtTanayoS.getText());

                Boolean abierta = vista.checkBoxAbiertaS.isSelected();
                if(abierta == null){ abierta = false;}
                nuevaSeccion.setAbierta(abierta);

                modelo.altaSeccion(nuevaSeccion);
                borrarCamposSecciones();
                break;

            case "Listar Secciones":
                listarSecciones(modelo.getSecciones());
                break;

            case "Modificar Seccion":
                Seccion seccionSeleccion = (Seccion)vista.listSecciones.getSelectedValue();
                seccionSeleccion.setNombre(vista.txtNombreS.getText());
                seccionSeleccion.setTamanyo(vista.txtTanayoS.getText());

                abierta = vista.checkBoxAbiertaS.isSelected();
                if(abierta == null){ abierta = false;}
                seccionSeleccion.setAbierta(abierta);

                modelo.modificarSeccion(seccionSeleccion);
                borrarCamposSecciones();
                break;

            case "Borrar Seccion":
                Seccion seccionBorrada  = (Seccion)vista.listSecciones.getSelectedValue();
                modelo.borrarSeccion(seccionBorrada);
                listarSecciones(modelo.getSecciones());
                break;
        }


        listarProductos(modelo.getProductos());
        listarSecciones(modelo.getSecciones());
        listarEmpresas(modelo.getEmpresas());
        listarMarcas(modelo.getMarcas());

        refrescarMarcas();
        refrescarEmpresas();
    }


    private void borrarCamposProductos() {
        vista.txtIdP.setText("");
        vista.comboBoxMarcaP.setSelectedIndex(-1);
        vista.comboBoxEmpresaP.setSelectedIndex(-1);
        vista.txtNombreP.setText("");
        vista.txtCodigoP.setText("");
        vista.txtPrecioP.setText("");
        vista.fechaCaducidadP.setText("");
    }

    private void borrarCamposSecciones() {
        vista.txtIdS.setText("");
        vista.txtNombreS.setText("");
        vista.txtTanayoS.setText("");
        vista.checkBoxAbiertaS.setSelected(false);
    }

    private void borrarCamposMarcas() {
        vista.txtIdM.setText("");
        vista.txtNombreM.setText("");
        vista.txtOrigenM.setText("");
        vista.fechaCreacionM.setText("");
        vista.txtPaisM.setText("");
    }

    private void borrarCamposEmpresas() {
        vista.txtIdE.setText("");
        vista.txtNombreE.setText("");
        vista.txtEmailE.setText("");
        vista.txtTelefonoE.setText("");
        vista.txtTipoEmpresaE.setText("");
        vista.txtWebE.setText("");
    }

    public void listarProductos(ArrayList<Producto> lista){
        vista.dlmProductos.clear();
        for(Producto unProducto : lista){
            vista.dlmProductos.addElement(unProducto);
        }
    }

    private void listarEmpresas(ArrayList<Empresa> empresas) {
        vista.dlmEmpresas.clear();
        for(Empresa empresa : empresas){
            vista.dlmEmpresas.addElement(empresa);
        }
    }
    private void listarMarcas(ArrayList<Marca> marcas) {
        vista.dlmMarcas.clear();
        for(Marca marca : marcas){
            vista.dlmMarcas.addElement(marca);
        }
    }

    private void listarSecciones(ArrayList<Seccion> secciones) {
        vista.dlmSecciones.clear();
        for(Seccion seccion : secciones){
            vista.dlmSecciones.addElement(seccion);
        }
    }

    public void listarProductosEmpresa(List<Producto> lista){
        vista.dlmProductosEmpresa.clear();
        for(Producto unProducto : lista){
            vista.dlmProductosEmpresa.addElement(unProducto);
        }
    }

    public void listarProductosMarca(List<Producto> lista){
        vista.dlmProductosMarca.clear();
        for(Producto unProducto : lista){
            vista.dlmProductosMarca.addElement(unProducto);
        }
    }

    /**
     * Actualiza las marcas que se ven en la lista y los comboboxes
     */
    private void refrescarMarcas() {
        listarMarcas(modelo.getMarcas());
        vista.comboBoxMarcaP.removeAllItems();
        for(int i = 0; i < vista.listMarcas.getLastVisibleIndex()+1; i++) {
            vista.comboBoxMarcaP.addItem(vista.dlmMarcas.getElementAt(i));
        }
    }

    /**
     * Actualiza las empresas que se ven en la lista y los comboboxes
     */
    private void refrescarEmpresas() {
        listarEmpresas(modelo.getEmpresas());
        vista.comboBoxEmpresaP.removeAllItems();
        for(int i = 0; i < vista.listEmpresas.getLastVisibleIndex()+1; i++) {
            vista.comboBoxEmpresaP.addItem(vista.dlmEmpresas.getElementAt(i));
        }
    }

    private void addActionListeners(ActionListener listener){
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);

        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.altaProductoButton.addActionListener(listener);
        vista.borrarProductoButton.addActionListener(listener);
        vista.modificarProductoButton.addActionListener(listener);
        vista.listarProductosButton.addActionListener(listener);

        vista.altaSeccionButton.addActionListener(listener);
        vista.borrarSeccionButton.addActionListener(listener);
        vista.modificarSeccionButton.addActionListener(listener);
        vista.listarSeccionesButton.addActionListener(listener);

        vista.altaMarcaButton.addActionListener(listener);
        vista.borrarMarcaButton.addActionListener(listener);
        vista.modificarMarcaButton.addActionListener(listener);
        vista.listarMarcasButton.addActionListener(listener);

        vista.altaEmpresaButton.addActionListener(listener);
        vista.borrarEmpresaButton.addActionListener(listener);
        vista.modificarEmpresaButton.addActionListener(listener);
        vista.listarEmpresasButton.addActionListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listEmpresas.addListSelectionListener(listener);
        vista.listMarcas.addListSelectionListener(listener);
        vista.listSecciones.addListSelectionListener(listener);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listProductos) {
                Producto productoSeleccion = (Producto) vista.listProductos.getSelectedValue();
                vista.txtIdP.setText(String.valueOf(productoSeleccion.getId()));
                vista.txtNombreP.setText(productoSeleccion.getNombre());
                vista.txtCodigoP.setText(productoSeleccion.getCodigo());
                vista.txtPrecioP.setText(String.valueOf(productoSeleccion.getPrecio()));
                vista.fechaCaducidadP.setDate(productoSeleccion.getFechacaducidad().toLocalDate());

                vista.comboBoxEmpresaP.setSelectedItem(productoSeleccion.getEmpresa()); //Seleccionamos empresa del producto
                vista.comboBoxMarcaP.setSelectedItem(productoSeleccion.getMarca()); //Seleccionamos marca del producto
            }else if(e.getSource() == vista.listEmpresas){
                Empresa empresaSeleccion = (Empresa)vista.listEmpresas.getSelectedValue();
                listarProductosEmpresa(modelo.getProductosEmpresa(empresaSeleccion));

                vista.txtIdE.setText(String.valueOf(empresaSeleccion.getId()));
                vista.txtNombreE.setText(empresaSeleccion.getNombre());
                vista.txtEmailE.setText(empresaSeleccion.getEmail());
                vista.txtTelefonoE.setText(empresaSeleccion.getTelefono());
                vista.txtTipoEmpresaE.setText(String.valueOf(empresaSeleccion.getTipoempresa()));
                vista.txtWebE.setText(empresaSeleccion.getWeb());
            }else if(e.getSource() == vista.listMarcas){
                Marca marcaSeleccion = (Marca)vista.listMarcas.getSelectedValue();
                listarProductosMarca(modelo.getProductosMarca(marcaSeleccion));

                vista.txtIdM.setText(String.valueOf(marcaSeleccion.getId()));
                vista.txtNombreM.setText(marcaSeleccion.getNombre());
                vista.txtOrigenM.setText(marcaSeleccion.getOrigen());
                vista.fechaCreacionM.setDate(marcaSeleccion.getFechacreacion().toLocalDate());
                vista.txtPaisM.setText(marcaSeleccion.getPais());
            }else if(e.getSource() == vista.listSecciones){
                Seccion seccionSeleccion = (Seccion)vista.listSecciones.getSelectedValue();
                vista.txtIdS.setText(String.valueOf(seccionSeleccion.getId()));
                vista.txtNombreS.setText(seccionSeleccion.getNombre());
                vista.txtTanayoS.setText(seccionSeleccion.getTamanyo());
                vista.checkBoxAbiertaS.setSelected(seccionSeleccion.isAbierta());
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
