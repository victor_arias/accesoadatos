package com.victor.practica2ud3.gui;

import com.victor.practica2ud3.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {
    SessionFactory sessionFactory;
    String user = "root";
    String pass = "";

    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        configuracion.setProperty(Environment.USER,user);
        configuracion.setProperty(Environment.PASS,pass);

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Empresa.class);
        configuracion.addAnnotatedClass(Marca.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Seccion.class);
        configuracion.addAnnotatedClass(SeccionProducto.class);


        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /*Empresa*/
    public void altaEmpresa(Empresa nuevaEmpresa) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaEmpresa);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Empresa> getEmpresas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Empresa");
        ArrayList<Empresa> lista = (ArrayList<Empresa>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarEmpresa(Empresa empresaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(empresaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarEmpresa(Empresa empresaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(empresaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /*Marca*/
    public void altaMarca(Marca nuevaMarca) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaMarca);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Marca> getMarcas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Marca");
        ArrayList<Marca> lista = (ArrayList<Marca>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarMarca(Marca marcaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(marcaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarMarca(Marca marcaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(marcaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /*Seccion*/
    public void altaSeccion(Seccion nuevaSeccion) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaSeccion);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Seccion> getSecciones() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Seccion");
        ArrayList<Seccion> lista = (ArrayList<Seccion>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarSeccion(Seccion seccionSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(seccionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarSeccion(Seccion seccionBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(seccionBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /*Producto*/
    public void altaProducto(Producto nuevoProducto) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Producto> getProductos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto");
        ArrayList<Producto> lista = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarProducto(Producto productoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(productoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarProducto(Producto productoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(productoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /******************************************************************************/
    public ArrayList<Producto> getProductosEmpresa(Empresa empresaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE empresa = :emp");
        query.setParameter("emp", empresaSeleccionada);
        ArrayList<Producto> lista = (ArrayList<Producto>) query.getResultList();
        sesion.close();
        return lista;
    }

    public ArrayList<Producto> getProductosMarca(Marca marcaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE marca = :mrc");
        query.setParameter("mrc", marcaSeleccionada);
        ArrayList<Producto> lista = (ArrayList<Producto>) query.getResultList();
        sesion.close();
        return lista;
    }
}
