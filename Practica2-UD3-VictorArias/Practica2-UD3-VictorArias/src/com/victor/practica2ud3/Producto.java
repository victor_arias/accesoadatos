package com.victor.practica2ud3;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "productos", schema = "supermercado", catalog = "")
public class Producto {
    private int id;
    private String nombre;
    private String codigo;
    private double precio;
    private Date fechacaducidad;
    private Marca marca;
    private Empresa empresa;
    private List<SeccionProducto> seccionproducto;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fechacaducidad")
    public Date getFechacaducidad() {
        return fechacaducidad;
    }

    public void setFechacaducidad(Date fechacaducidad) {
        this.fechacaducidad = fechacaducidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                Double.compare(producto.precio, precio) == 0 &&
                Objects.equals(nombre, producto.nombre) &&
                Objects.equals(codigo, producto.codigo) &&
                Objects.equals(fechacaducidad, producto.fechacaducidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, codigo, precio, fechacaducidad);
    }

    @ManyToOne
    @JoinColumn(name = "id_marca", referencedColumnName = "id", nullable = false)
    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    @ManyToOne
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", nullable = false)
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    @OneToMany(mappedBy = "producto")
    public List<SeccionProducto> getSeccionproducto() {
        return seccionproducto;
    }

    public void setSeccionproducto(List<SeccionProducto> seccionproducto) {
        this.seccionproducto = seccionproducto;
    }

    @Override
    public String toString() {
        return nombre + "-" + codigo;
    }
}
