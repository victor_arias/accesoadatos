package com.victor.practica2ud3;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "marcas", schema = "supermercado", catalog = "")
public class Marca {
    private int id;
    private String nombre;
    private String origen;
    private Date fechacreacion;
    private String pais;
    private List<Producto> productos;
    private List<Empresa> empresas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "origen")
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Basic
    @Column(name = "fechacreacion")
    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Marca marca = (Marca) o;
        return id == marca.id &&
                Objects.equals(nombre, marca.nombre) &&
                Objects.equals(origen, marca.origen) &&
                Objects.equals(fechacreacion, marca.fechacreacion) &&
                Objects.equals(pais, marca.pais);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, origen, fechacreacion, pais);
    }

    @OneToMany(mappedBy = "marca")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @ManyToMany
    @JoinTable(name = "empresa_marca", catalog = "", schema = "supermercado", joinColumns = @JoinColumn(name = "id_empresa", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_marca", referencedColumnName = "id", nullable = false))
    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    @Override
    public String toString() {
        return nombre + "-" + origen;
    }
}
