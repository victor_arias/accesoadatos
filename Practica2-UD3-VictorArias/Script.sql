CREATE DATABASE if not exists supermercado;
--
USE supermercado;
--
create table if not exists marcas (
										id int auto_increment primary key,
										nombre varchar(50) not null,
										origen varchar(150) not null,
										fechacreacion date,
										pais varchar(50));
--
create table if not exists empresas (
                                        id int auto_increment primary key,
                                        nombre varchar(50) not null,
                                        email varchar(100) not null,
                                        telefono varchar(9),
                                        tipoempresa varchar(50),
                                        web varchar(500));
--
create table if not exists productos(
                                        id int auto_increment primary key,
                                        id_marca int not null,
                                        id_empresa int not null,
                                        nombre varchar(50) not null,
                                        codigo varchar(40) not null UNIQUE,
                                        precio float not null,
                                        fechacaducidad date);
--

create table if not exists secciones(
                                        id int auto_increment primary key,
                                        nombre varchar(50) not null,
                                        tamanyo varchar(50) not null,
                                        abierta tinyint not null);
--
										
create table if not exists empresa_marca
								
(
	id_empresa INT UNSIGNED REFERENCES empresas,
	id_marca INT UNSIGNED REFERENCES marcas,
	PRIMARY KEY (id_empresa, id_marca)
);
--

create table if not exists seccion_producto
(
	id_seccion INT UNSIGNED REFERENCES secciones,
	id_producto INT UNSIGNED REFERENCES productos,
	cantidad INTEGER DEFAULT 0,
	PRIMARY KEY (id_seccion, id_producto)
);
--
