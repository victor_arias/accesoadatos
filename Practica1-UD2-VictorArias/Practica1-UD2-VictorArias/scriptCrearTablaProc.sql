DELIMITER //
CREATE PROCEDURE crearTablaProductos()
BEGIN

    CREATE TABLE productos(
                              id int primary key auto_increment,
                              idProducto varchar(40) unique,
                              precio float,
                              seccion varchar (40),
                              fechaCaducidad timestamp
    );

END //
