package com.victorarias.practica1.gui;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created by Victor Arias Benito
 */
public class Controlador implements ActionListener, TableModelListener {
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;
    
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);

        vista.txtUsuario.setText(modelo.getUser());
        vista.passwordField1.setText(modelo.getPassword());
    }

    private void addActionListener(ActionListener listener){
        vista.buscarButton.addActionListener(listener);
        vista.eliminarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.productosPorSeccionButton.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);

        vista.cambiarDatosConexion.addActionListener(listener);
    }

    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
    }

    private void iniciarTabla() {
        String[] headers={"id","id del producto","precio","seccion","fecha de caducidad"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void iniciarTabla1() {
        String[] headers={"Cuantos","Seccion"};
        vista.dtm1.setColumnIdentifiers(headers);
    }


    /**
     * Controla las acciones de cada boton de la app
     * @param e Es el boton que ha sido pulsado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Cambiar usuario y pass":
                modelo.setPropValues(vista.txtUsuario.getText(),String.valueOf(vista.passwordField1.getPassword()));
                break;

            case "Productos por seccion":
                try {
                    modelo.productosPorSeccion();
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }


                break;

            case "CrearTablaProductos":
                try {
                    modelo.crearTablaProductos();
                    vista.lblAccion.setText("Tabla productos creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Nuevo":
                try {
                    modelo.insertarProducto(vista.txtIdProducto.getText(),Double.parseDouble(vista.txtPrecio.getText()),vista.txtSeccion.getText(),vista.dateTimePicker.getDateTimePermissive());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                try {
                    cargarFilas(modelo.buscarProducto(vista.txtBuscar.getText()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Eliminar":
                try {
                    int filaBorrar=vista.tabla1.getSelectedRow();
                    int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarProducto(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Conectar":
                if (estado==tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        cargarFilas(modelo.obtenerDatos());
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }
                break;
        }
    }

    /**
     * Carga las filas de la tabla el ResultSet de la base de datos
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[5];
        vista.dtm.setRowCount(0); //vaciamos tabla

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }
    }

    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm1.setRowCount(0); //vaciamos tabla

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }
    }


    /**
     * Funcion que recoge los cambios realizados sobre la tabla clicando sobre ella etc...
     * @param e Tipo de acutalizacion de la tabla
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarProducto(Integer.parseInt(String.valueOf(vista.dtm.getValueAt(filaModicada,0))),(String)vista.dtm.getValueAt(filaModicada,1),(Double) Double.parseDouble(String.valueOf(vista.dtm.getValueAt(filaModicada,2))) ,(String)vista.dtm.getValueAt(filaModicada,3), Timestamp.valueOf(String.valueOf(vista.dtm.getValueAt(filaModicada,4))));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
}
