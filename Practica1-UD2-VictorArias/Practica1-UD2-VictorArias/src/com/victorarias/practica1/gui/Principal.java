package com.victorarias.practica1.gui;

/**
 * Created by Victor Arias Benito
 */
public class Principal {
    public static void main(String args[]) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
