package com.victorarias.practica1.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame {
    private JPanel panel1;
    JTextField txtIdProducto;
    JTextField txtPrecio;
    JTextField txtSeccion;
    JButton buscarButton;
    JTextField txtBuscar;
    JButton nuevoButton;
    JButton eliminarButton;
    JTable tabla1;
    JTable tabla2;
    JButton productosPorSeccionButton;
    JLabel lblAccion;
    DateTimePicker dateTimePicker;
    JTextField txtUsuario;
    JPasswordField passwordField1;
    JButton cambiarDatosConexion;

    DefaultTableModel dtm;
    DefaultTableModel dtm1;
    JMenuItem itemConectar;
    JMenuItem itemCrearTabla;
    JMenuItem itemSalir;
    JFrame frame;

    public Vista(){
        frame = new JFrame("Productos BBDD");
        frame.setBounds(300,200,1000,750);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm =new DefaultTableModel();
        tabla1.setModel(dtm);

        dtm1 =new DefaultTableModel();
        tabla2.setModel(dtm1);

        crearMenu();

        frame.setVisible(true);
    }

    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla Productos");
        itemCrearTabla.setActionCommand("CrearTablaProductos");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }

}
