package com.victorarias.practica1.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Created by Victor Arias Benito
 */
public class Modelo {
    private Connection conexion;
    private String user;
    private String password;

    Modelo(){
        getPropValues();
    }

    /**
     * Función que actualiza un producto en la base de datos con la id indicada
     * @param id int con valor del producto
     * @param idProducto String con valor del producto
     * @param precio Double con valor del producto
     * @param seccion String con valor del producto
     * @param fechaCaducidad Timestamp con valor del producto
     * @return
     * @throws SQLException
     */
    public int modificarProducto (int id, String idProducto, Double precio, String seccion, Timestamp fechaCaducidad) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE productos SET idProducto=?, precio=?,"+
                "seccion=?,fechaCaducidad=? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,idProducto);
        sentencia.setDouble(2,precio);
        sentencia.setString(3,seccion);
        sentencia.setTimestamp(4,fechaCaducidad);
        sentencia.setInt(5,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;

    }

    /**
     * Funcion que debuelve un resulset con los datos necesarios
     * @param idProducto String de identificación del producto
     * @return Debuelve los datos de un producto si este existe y sino debuelve null
     * @throws SQLException
     */
    public ResultSet buscarProducto(String idProducto) throws SQLException {
        if(idProducto != null){
            String sentenciaSql = "SELECT * FROM productos WHERE idProducto = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, idProducto);
            ResultSet resultado = sentencia.executeQuery();
            return resultado;
        }else{
            return null;
        }
    }

    /**
     * No se ha utilizado
     * @throws SQLException
     */
    public void productosPorSeccion() throws SQLException {
        String sentenciaSql="call mostrarProductosPorSeccion()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Obtiene los datos a insertar en la tabla de arriba
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM productos";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Obtiene los datos a insertar en la tabla de abajo
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT seccion,COUNT(*) FROM productos GROUP BY seccion";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Crea una tabla productos en la base de datos
     * @throws SQLException
     */
    public void crearTablaProductos() throws SQLException {
        conectar();

        String sentenciaSql="call crearTablaProductos()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/supermercado", user, password);
    }

    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    /**
     * Funcion que inserta un producto en la base de datos
     * @param idProducto Identificacion del producto
     * @param precio Double con el precio del producto
     * @param seccion Seccion en la que se encuentra el producto
     * @param fechaCaducidad Dato de tipo LocalDateTime con la fecha de caducidad del producto
     * @return
     * @throws SQLException
     */
    public int insertarProducto(String idProducto, Double precio, String seccion, LocalDateTime fechaCaducidad) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO productos(idProducto, precio, seccion, fechaCaducidad)"+
                "VALUES (?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,idProducto);
        sentencia.setDouble(2,precio);
        sentencia.setString(3,seccion);
        sentencia.setTimestamp(4,Timestamp.valueOf(fechaCaducidad));

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;
    }

    /**
     * Funcion que elimina un producto de la base de datos
     * @param id Es el identificador en la base de datos del producto
     * @return El resultado indica si la operacion fue un exito o no
     * @throws SQLException
     */
    public int eliminarProducto(int id) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="DELETE FROM productos WHERE id=?";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            user = prop.getProperty("user");
            password = prop.getProperty("pass");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param user      user de la bbdd
     * @param pass      contraseña de la bbdd
     */
    void setPropValues(String user, String pass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.user = user;
        this.password = pass;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
