package com.victorarias.practicaUD4.gui;


import com.victorarias.practicaUD4.base.Marca;
import com.victorarias.practicaUD4.base.Producto;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * Clase Controlador
 * @author vbeni
 * @see java.awt.event.ActionListener
 * @see java.awt.event.KeyListener
 * @see javax.swing.event.ListSelectionListener
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    //Campos
    Vista vista;
    Modelo modelo;

    /**
     * Constructor de Controlador
     * @param vista de tipo Vista
     * @param modelo de tipo Modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    /**
     * Método inicializar listeners
     */
    private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        modelo.conectar();
        listarProductos(modelo.getProductos());
        listarMarcas(modelo.getMarcas());
    }

    /**
     * Método addActionListeners(), añado los listeners a los botones
     * @param listener de tipo ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnNuevoProducto.addActionListener(listener);
        vista.btnModificarProducto.addActionListener(listener);
        vista.btnEliminarProducto.addActionListener(listener);
        vista.btnEliminarMarca.addActionListener(listener);
        vista.btnModificarMarca.addActionListener(listener);
        vista.btnNuevaMarca.addActionListener(listener);

    }

    /**
     * Método addListSelectionListeners(), añadir listeners a los JLists
     * @param listener de ListSelectionListener
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listMarcas.addListSelectionListener(listener);
    }

    /**
     * Método addKeyListeners(), añadir listeners a los campos de buscar
     * @param listener de KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscarP.addKeyListener(listener);
        vista.txtBuscarM.addKeyListener(listener);
    }

    /**
     * Método actionPerformed(), implementado por ActionListener, donde asignas las acciones a los botones
     * @param e de ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Producto unProducto;
        Marca unaMarca;
        switch (comando){
            case "Nuevo Producto":
                unProducto = new Producto();
                modificarProductoFromCampos(unProducto);
                modelo.guardarProducto(unProducto);
                listarProductos(modelo.getProductos());
                break;
            case "Modificar Producto":
                unProducto = (Producto) vista.listProductos.getSelectedValue();
                modificarProductoFromCampos(unProducto);
                modelo.modificarProducto(unProducto);

                listarProductos(modelo.getProductos());
                break;
            case "Eliminar Producto":
                unProducto = (Producto) vista.listProductos.getSelectedValue();
                modelo.borrarProducto(unProducto);
                listarProductos(modelo.getProductos());
                break;
            case "Nueva Marca":
                unaMarca = new Marca();
                modificarMarcaFromCampos(unaMarca);
                modelo.guardarMarca(unaMarca);
                listarMarcas(modelo.getMarcas());
                break;
            case "Modificar Marca":
                unaMarca = (Marca) vista.listMarcas.getSelectedValue();
                modificarMarcaFromCampos(unaMarca);
                modelo.modificarMarca(unaMarca);

                listarMarcas(modelo.getMarcas());
                break;
            case "Eliminar Marca":
                unaMarca = (Marca) vista.listMarcas.getSelectedValue();
                modelo.borrarMarca(unaMarca);
                listarMarcas(modelo.getMarcas());
                break;
        }
    }

    /**
     * Método listarProductos(), listo los productos
     * @param lista de List<Producto>
     */
    private void listarProductos(List<Producto> lista){
        vista.dlmProductos.clear();
        for (Producto producto : lista){
            vista.dlmProductos.addElement(producto);
        }
    }
    /**
     * Método listarMarcas(), listo las marcas
     * @param lista de List<Marca>
     */
    private void listarMarcas(List<Marca> lista){
        vista.dlmMarcas.clear();
        for (Marca marca : lista){
            vista.dlmMarcas.addElement(marca);
        }
    }

    /**
     * Método modificarProductoFromCampos(), para modificar los datos
     * @param unProducto de tipo Producto
     */
    private void modificarProductoFromCampos(Producto unProducto) {
        unProducto.setNombre(vista.txtNombreP.getText());
        unProducto.setCodigo(vista.txtCodigoP.getText());
        unProducto.setPrecio(Double.parseDouble(vista.txtPrecioP.getText()));
        unProducto.setFechaCaducidad(vista.datePickerP.getDate());
    }
    /**
     * Método modificarMarcaFromCampos(), para modificar los datos
     * @param unaMarca de tipo Marca
     */
    private void modificarMarcaFromCampos(Marca unaMarca) {
        unaMarca.setNombre(vista.txtNombreM.getText());
        unaMarca.setPais(vista.txtPaisM.getText());
        unaMarca.setPrecioAccion(Double.parseDouble(vista.txtPrecioAccionM.getText()));
        unaMarca.setFechaCreacion(vista.datePickerM.getDate());
    }

    /**
     * Método valueChanged(), que al pinchar en un registro, me muestre sus datos en sus campos
     * @param e de ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            Producto unProducto = (Producto) vista.listProductos.getSelectedValue();
            vista.txtNombreP.setText(unProducto.getNombre());
            vista.txtCodigoP.setText(unProducto.getCodigo());
            vista.txtPrecioP.setText(String.valueOf(unProducto.getPrecio()));
            vista.datePickerP.setDate(unProducto.getFechaCaducidad());
        }
        if(e.getValueIsAdjusting()){
            Marca unaMarca = (Marca) vista.listMarcas.getSelectedValue();
            vista.txtNombreM.setText(unaMarca.getNombre());
            vista.txtPaisM.setText(unaMarca.getPais());
            vista.txtPrecioAccionM.setText(String.valueOf(unaMarca.getPrecioAccion()));
            vista.datePickerM.setDate(unaMarca.getFechaCreacion());
        }
    }

    /**
     * Método keyReleased(), para listar según lo que buscas
     * @param e de KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscarP){
            listarProductos(modelo.getProductos(vista.txtBuscarP.getText()));
        }
        if(e.getSource() == vista.txtBuscarM){
            listarMarcas(modelo.getMarca(vista.txtBuscarM.getText()));
        }
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }




}
