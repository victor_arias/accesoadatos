package com.victorarias.practicaUD4.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.victorarias.practicaUD4.base.Marca;
import com.victorarias.practicaUD4.base.Producto;

import javax.swing.*;

/**
 * Clase Vista
 * @author Vbenito
 */
public class Vista {
    //Campos
    private JPanel panel1;
    private JTabbedPane tabbedPane1;

    //Producto
    JList listProductos;
    JTextField txtNombreP;
    JTextField txtCodigoP;
    JTextField txtPrecioP;
    JTextField txtBuscarP;
    DatePicker datePickerP;
    JButton btnNuevoProducto;
    JButton btnModificarProducto;
    JButton btnEliminarProducto;

    //Marca
    JList listMarcas;
    JTextField txtNombreM;
    JTextField txtPaisM;
    JTextField txtPrecioAccionM;
    JTextField txtBuscarM;
    DatePicker datePickerM;
    JButton btnNuevaMarca;
    JButton btnModificarMarca;
    JButton btnEliminarMarca;


    DefaultListModel<Producto> dlmProductos;
    DefaultListModel<Marca> dlmMarcas;

    /**
     * Constructor de Vista()
     */
    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setBounds(500,200,800,600);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        inicializar();
    }

    /**
     * inicializar(),inicializar modelos
     */
    private void inicializar(){
        dlmProductos = new DefaultListModel<Producto>();
        listProductos.setModel(dlmProductos);

        dlmMarcas = new DefaultListModel<Marca>();
        listMarcas.setModel(dlmMarcas);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
