package com.victorarias.practicaUD4.gui;


import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.victorarias.practicaUD4.base.Marca;
import com.victorarias.practicaUD4.base.Producto;
import com.victorarias.practicaUD4.util.Util;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Clase Modelo
 * @author
 */
public class Modelo {

    //Campos
    private final static String COLECCION_PRODUCTOS = "Productos";
    private final static String COLECCION_MARCAS = "Marcas";
    private final static String DATABASE = "Supermercado";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionProductos;
    private MongoCollection coleccionMarcas;

    /**
     * Método conectar(), conectas con la base de datos
     */
    public void conectar() {
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionProductos = baseDatos.getCollection(COLECCION_PRODUCTOS);
        coleccionMarcas=baseDatos.getCollection(COLECCION_MARCAS);
    }

    /**
     * Método desconectar(), para desconectar la base de datos
     */
    public void desconectar(){
        client.close();

    }

    /**
     * Método guardarProducto(), para guardar Productos en la base de datos
     * @param unProducto de tipo Producto
     */
    public void guardarProducto(Producto unProducto) {
        coleccionProductos.insertOne(productoToDocument(unProducto));

    }

    /**
     * Método guardarMarca(), para guardar marca en la base de datos
     * @param unaMarca de tipo Marca
     */
    public void guardarMarca(Marca unaMarca) {
        coleccionMarcas.insertOne(marcaToDocument(unaMarca));

    }

    /**
     * Método getProductos(), lista productos
     * @return lista
     */
    public List<Producto> getProductos(){
        ArrayList<Producto> lista = new ArrayList<>();

        Iterator<Document> it = coleccionProductos.find().iterator();
        while(it.hasNext()){
            lista.add(documentToProducto(it.next()));
        }
        return lista;
    }

    /**
     * Método getMarcas(), lista marcas
     * @return lista
     */
    public List<Marca> getMarcas(){
        ArrayList<Marca> lista = new ArrayList<>();

        Iterator<Document> it = coleccionMarcas.find().iterator();
        while(it.hasNext()){
            lista.add(documentToMarca(it.next()));
        }
        return lista;
    }


    /**
     * Método getProductos(),listo los productos atendiendo a 2 criterios basados en expresiones regulares.
     * @param text de tipo String
     * @return lista
     */
    public List<Producto> getProductos(String text) {
        ArrayList<Producto> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("codigo", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionProductos.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToProducto(iterator.next()));
        }

        return lista;
    }
    /**
     * Método getMarca(),listo los marcas atendiendo a 2 criterios basados en expresiones regulares.
     * @param text de tipo String
     * @return lista
     */
    public List<Marca> getMarca(String text) {
        ArrayList<Marca> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("pais", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionMarcas.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToMarca(iterator.next()));
        }

        return lista;
    }

    /**
     * Método productoToDocument() de tipo Document
     * @param unProducto de tipo Producto
     * @return documento
     */
    public Document productoToDocument(Producto unProducto){
        Document documento = new Document();
        documento.append("nombre", unProducto.getNombre());
        documento.append("codigo", unProducto.getCodigo());
        documento.append("precio", unProducto.getPrecio());
        documento.append("fechaCaducidad", Util.formatearFecha(unProducto.getFechaCaducidad()));
        return documento;
    }
    /**
     * Método marcaToDocument() de tipo Document
     * @param unaMarca de tipo Marca
     * @return documento
     */
    public Document marcaToDocument(Marca unaMarca){
        Document documento = new Document();
        documento.append("nombre", unaMarca.getNombre());
        documento.append("pais", unaMarca.getPais());
        documento.append("precioAccion", unaMarca.getPrecioAccion());
        documento.append("fechaCreacion", Util.formatearFecha(unaMarca.getFechaCreacion()));
        return documento;
    }

    /**
     * Método documentToProducto(), registro de las filas de producto
     * @param document de tipo Document
     * @return unProducto
     */
    public Producto documentToProducto(Document document){
        Producto unProducto = new Producto();
        unProducto.setId(document.getObjectId("_id"));
        unProducto.setNombre(document.getString("nombre"));
        unProducto.setCodigo(document.getString("codigo"));
        unProducto.setPrecio(document.getDouble("precio"));
        unProducto.setFechaCaducidad(Util.parsearFecha(document.getString("fechaCaducidad")));
        return unProducto;
    }
    /**
     * Método documentToMarca(), registro de las filas de marca
     * @param document de tipo Document
     * @return unaMarca
     */
    public Marca documentToMarca(Document document){
        Marca unaMarca = new Marca();
        unaMarca.setId(document.getObjectId("_id"));
        unaMarca.setNombre(document.getString("nombre"));
        unaMarca.setPais(document.getString("pais"));
        unaMarca.setPrecioAccion(document.getDouble("precioAccion"));
        unaMarca.setFechaCreacion(Util.parsearFecha(document.getString("fechaCreacion")));
        return unaMarca;
    }

    /**
     * Método modificarProducto(), sirve para modificar productos
     * @param unProducto de Producto
     */
    public void modificarProducto(Producto unProducto) {
        coleccionProductos.replaceOne(new Document("_id", unProducto.getId()), productoToDocument(unProducto));
    }

    /**
     * Método modificarMarca(), sirve para modificar marcas
     * @param unaMarca de Marca
     */
    public void modificarMarca(Marca unaMarca) {
        coleccionMarcas.replaceOne(new Document("_id", unaMarca.getId()), marcaToDocument(unaMarca));
    }

    /**
     * Método borrarProducto(), para borrar un producto
     * @param unProducto de Producto
     */
    public void borrarProducto(Producto unProducto) {
        coleccionProductos.deleteOne(productoToDocument(unProducto));
    }
    /**
     * Método borrarMarca(), para borrar un marca
     * @param unaMarca de Marca
     */
    public void borrarMarca(Marca unaMarca) {
        coleccionMarcas.deleteOne(marcaToDocument(unaMarca));

    }
}
