package com.victorarias.practicaUD4.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Marca {
    //Campos
    private ObjectId id;
    private String nombre;
    private String pais;
    private double precioAccion;
    private LocalDate fechaCreacion;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public double getPrecioAccion() {
        return precioAccion;
    }

    public void setPrecioAccion(double precioAccion) {
        this.precioAccion = precioAccion;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public String toString() {
        return nombre + ":" + pais;
    }
}
