package com.victorarias.practica2.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.victorarias.practica2.base.Producto;

import javax.swing.*;

/**
 * Clase Ventana
 * Contiene los componentes visuales de la app
 */
public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton comestibleRadioButton;
    public JRadioButton noComestibleRadioButton;
    public JTextField nombreTxt;
    public JTextField marcaTxt;
    public JTextField precioTxt;
    public JLabel cambianteJL;
    public JTextField diasGarantiaTxt;
    public DatePicker fechaCaducidadDPicker;
    public JButton nuevoButton;
    public JButton exportarButton;
    public JButton importarButton;
    public JButton borrarSeleccionadoButton;
    public JList list1;
    public JButton borrarTodosButton;


    //Elementos creados por mi
    public DefaultListModel<Producto> dlmProducto;

    /**
     * Constructor de Ventana
     */
    public Ventana() {
        frame = new JFrame("Productos MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    /**
     * Inicializa un DefaultListModel de Productos
     */
    private void initComponents() {
        dlmProducto=new DefaultListModel<Producto>();
        list1.setModel(dlmProducto);
    }

}
