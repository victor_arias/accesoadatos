package com.victorarias.practica2.gui;


import com.victorarias.practica2.base.Comestible;
import com.victorarias.practica2.base.NoComestible;
import com.victorarias.practica2.base.Producto;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Modelo
 * En este apartado se han creado funciones para dar funcionalidad a los botones de la aplicacion
 */
public class ProductosModelo {
    private ArrayList<Producto> listaProductos;

    /**
     * Constructor del Modelo
     * Genera ina lista de productos
     */
    public ProductosModelo() {
        listaProductos = new ArrayList<Producto>();
    }

    /**
     * Funcion que obtiene los productos de la lista del Modelo
     * @return listaProductos Una lista con datos del tipo Producto
     */
    public ArrayList<Producto> obtenerProductos() {
        return listaProductos;
    }

    /**
     * Funcion que añade un dato de la clase Comestible a la lista de Productos
     * @param nombre Un String con el nombre del Producto
     * @param marca Un String con el nombre de la marca del producto
     * @param precio Un double con el precio del Producto
     * @param fechaDeCaducidad Un LocalDate con la fecha de caducidad del Producto
     */
    public void altaComestible(String nombre, String marca, double precio, LocalDate fechaDeCaducidad) {
        Comestible nuevoComestible=new Comestible(nombre,marca, precio,fechaDeCaducidad);
        listaProductos.add(nuevoComestible);
    }

    /**
     * Funcion que añade un dato de la clase NoComestible a la lista de Productos
     * @param nombre Un String con el nombre del Producto
     * @param marca Un String con el nombre de la marca del Producto
     * @param precio Un double con el precio del Producto
     * @param diasDeGarantia Un Integer con los disas de garantia del Producto
     */
    public void altaNoComestible(String nombre, String marca, double precio, int diasDeGarantia) {
        NoComestible nuevoNoComestible= new NoComestible(nombre,marca,precio,diasDeGarantia);
        listaProductos.add(nuevoNoComestible);
    }

    /**
     * Funcion que elimina un Producto de la lista de productos
     * @param nombre String nombre del producto que se va a eliminar de la lista de productos
     */
    public void borrarProducto(String nombre){
        if(existeNombre(nombre)){
            for (Producto unProducto:listaProductos) {
                if(unProducto.getNombre().equals(nombre)) {
                    listaProductos.remove(unProducto);
                    break;
                }
            }
        }
    }

    /**
     * Funcion que elimina todos los productos de la lista de productos
     */
    public void borrarTodos(){
        listaProductos.clear();
    }

    /**
     * Funcion que devuelve true si existe un Producto con el nombre entregado y false en caso contrario
     * @param nombre Un String con el nombre de un Producto
     * @return Devuelve un Boolean, True si existe el nombre entregado y False en caso contrario
     */
    public boolean existeNombre(String nombre) {
        for (Producto unProducto:listaProductos) {
            if(unProducto.getNombre().equals(nombre)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Funcion que genera un fichero XML con los datos del programa que podran ser adquiridos posteriormente
     * @param fichero Contiene un dato tipo File con los datos del lugar al que se va a exportar el fichero XML y el nombre del mismo
     * @throws ParserConfigurationException Lanza una excepcion propia de la generacion de ficheros
     * @throws TransformerException Lanza una excepcion proia de la generacion de ficheros
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Productos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoProducto = null, nodoDatos = null;
        Text texto = null;

        for (Producto unProducto : listaProductos) {

            /*Añado dentro de la etiqueta raiz (Productos) una etiqueta
            dependiendo del tipo de producto que este almacenando
            (comestible o noComestible)
             */
            if (unProducto instanceof Comestible) {
                nodoProducto = documento.createElement("Comestible");

            } else {
                nodoProducto = documento.createElement("NoComestible");
            }
            raiz.appendChild(nodoProducto);

            /*Dentro de la etiqueta producto le añado
            las subetiquetas con los datos de sus
            atributos (nombre, marca, etc)
             */
            nodoDatos = documento.createElement("nombre");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(unProducto.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("marca");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(unProducto.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("precio");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(String.valueOf(unProducto.getPrecio()));
            nodoDatos.appendChild(texto);




            /* Como hay un dato que depende del tipo de producto
            debo acceder a él controlando el tipo de objeto
             */
            if (unProducto instanceof Comestible) {
                nodoDatos = documento.createElement("fechaDeCaducidad");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(((Comestible) unProducto).getFechaDeCaducidad().toString());
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("diasDeGarantia");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(((NoComestible) unProducto).getDiasDeGarantia()));
                nodoDatos.appendChild(texto);
            }

        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /**
     * Funcion que importa un fichero XML con los datos del programa
     * @param fichero Dato File que contiene direccion, nombre, etc, del fichero
     * @throws ParserConfigurationException Lanza excepcion posible en la importacionXML
     * @throws IOException Lanza excepcion posible en la importacionXML
     * @throws SAXException Lanza excepcion posible en la importacionXML
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaProductos = new ArrayList<Producto>();
        Comestible nuevoComestible = null;
        NoComestible nuevoNoComestible= null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoProducto = (Element) listaElementos.item(i);


            if (nodoProducto.getTagName().equals("Comestible")) {
                nuevoComestible = new Comestible();
                nuevoComestible.setNombre(nodoProducto.getChildNodes().item(0).getTextContent());
                nuevoComestible.setMarca(nodoProducto.getChildNodes().item(1).getTextContent());
                nuevoComestible.setPrecio(Double.parseDouble(nodoProducto.getChildNodes().item(2).getTextContent()));
                nuevoComestible.setFechaDeCaducidad(LocalDate.parse(nodoProducto.getChildNodes().item(3).getTextContent()));

                listaProductos.add(nuevoComestible);

            } else if (nodoProducto.getTagName().equals("NoComestible")) {
                    nuevoNoComestible = new NoComestible();
                    nuevoNoComestible.setNombre(nodoProducto.getChildNodes().item(0).getTextContent());
                    nuevoNoComestible.setMarca(nodoProducto.getChildNodes().item(1).getTextContent());
                    nuevoNoComestible.setPrecio(Double.parseDouble(nodoProducto.getChildNodes().item(2).getTextContent()));
                    nuevoNoComestible.setDiasDeGarantia(Integer.parseInt(nodoProducto.getChildNodes().item(3).getTextContent()));

                    listaProductos.add(nuevoNoComestible);
                }

        }
    }
}
