package com.victorarias.practica2.gui;

import com.victorarias.practica2.base.Comestible;
import com.victorarias.practica2.base.NoComestible;
import com.victorarias.practica2.base.Producto;
import com.victorarias.practica2.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Clase Controlador
 * Se encarga de conectar la vista y el modelo
 */
public class ProductosControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private ProductosModelo modelo;
    private File ultimaRutaExportada;

    /**
     * Constructor del controlador
     * @param vista Recibe una Vista para trabajar con ella (parte visual de la app)
     * @param modelo Recibe un Modelo con funciones para trabajar
     */
    public ProductosControlador(Ventana vista, ProductosModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }


    /**
     * Funcion que se encarga de escuchar las acciones del usuario y actuar en consecuencia
     * @param e Indica donde se ha producido la llamada
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Tipo de producto\nNombre\nMarca\nPrercio\n" +
                            vista.cambianteJL.getText());
                    break;
                }

                if (modelo.existeNombre(vista.nombreTxt.getText())) {
                    Util.mensajeError("Ya existe un producto con ese nombre\n" +
                            vista.nombreTxt.getText());
                    break;
                }
                if (vista.comestibleRadioButton.isSelected()) {
                    modelo.altaComestible(vista.nombreTxt.getText(), vista.marcaTxt.getText(), Double.parseDouble(vista.precioTxt.getText()), vista.fechaCaducidadDPicker.getDate());
                } else {
                    modelo.altaNoComestible(vista.nombreTxt.getText(), vista.marcaTxt.getText(), Double.parseDouble(vista.precioTxt.getText()), Integer.parseInt(vista.diasGarantiaTxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "BorrarSeleccionado":
                modelo.borrarProducto(vista.nombreTxt.getText());
                limpiarCampos();
                refrescar();
                break;
            case "BorrarTodos":
                modelo.borrarTodos();
                limpiarCampos();
                refrescar();
                break;
            case "Comestible":
                vista.cambianteJL.setText("Fecha de caducidad");
                vista.diasGarantiaTxt.setVisible(false);
                vista.fechaCaducidadDPicker.setVisible(true);
                break;
            case "No Comestible":
                vista.cambianteJL.setText("Dias de garantia");
                vista.fechaCaducidadDPicker.setVisible(false);
                vista.diasGarantiaTxt.setVisible(true);
                break;

        }

    }

    /**
     * Funcion qe indica si hay campos vacios en el formulario
     * @return Devuelve true si hay campos vacios y false si no los hay
     */
    private boolean hayCamposVacios() {
        if (vista.nombreTxt.getText().isEmpty() ||
                vista.marcaTxt.getText().isEmpty() ||
                vista.precioTxt.getText().isEmpty() ||
                (vista.diasGarantiaTxt.getText().isEmpty() && vista.fechaCaducidadDPicker.getText().isEmpty()) ||
                (!vista.comestibleRadioButton.isSelected() && !vista.noComestibleRadioButton.isSelected())) {
            return true;
        }
        return false;
    }

    /**
     * Fuccion que limpoa los campos del formulario
     */
    private void limpiarCampos() {
        vista.nombreTxt.setText(null);
        vista.marcaTxt.setText(null);
        vista.precioTxt.setText(null);
        vista.diasGarantiaTxt.setText(null);
        vista.fechaCaducidadDPicker.setText(null);
        vista.nombreTxt.requestFocus();
    }

    /**
     * Funcion que refresca los Productos que se muestran
     */
    private void refrescar() {
        vista.dlmProducto.clear();
        for (Producto unProducto : modelo.obtenerProductos()) {
            vista.dlmProducto.addElement(unProducto);
        }
    }

    /**
     * Funcion encargada de añadir los ActionListener
     * @param listener Recibe un ActionListener
     */
    private void addActionListener(ActionListener listener) {
        vista.comestibleRadioButton.addActionListener(listener);
        vista.noComestibleRadioButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.borrarSeleccionadoButton.addActionListener(listener);
        vista.borrarTodosButton.addActionListener(listener);
    }

    /**
     * Funcio para añadir un WindowListener
     * @param listener Recibe un WindowListener
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * Funcion encargada de escuchar las acciones de una lista
     * @param listener Recibe un ListSelectionListener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * Funcion que carga los datos de configuracion de un fichero previamente existente
     * @throws IOException Lanza la excepcion en caso de que la haya
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("productos.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Funcion para actualizar los datos de configuracion del programa
     * @param ultimaRutaExportada Dato File con la ultima ruta a la que se ha exportado un fichero
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Funcion para guardar los datos de configuracion del programa
     * @throws IOException  Lanza la excepcion en caso de que la haya
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("productos.conf"), "Datos configuracion productos");

    }

    /**
     * Funcion que detecta cuando cambia el valor de una lista
     * @param e Recibe un ListSelectioEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Producto productoSeleccionado = (Producto) vista.list1.getSelectedValue();
            vista.nombreTxt.setText((productoSeleccionado.getNombre()));
            vista.marcaTxt.setText(productoSeleccionado.getMarca());
            vista.precioTxt.setText(String.valueOf(productoSeleccionado.getPrecio()));

            if (productoSeleccionado instanceof Comestible) {
                vista.comestibleRadioButton.doClick();
                vista.fechaCaducidadDPicker.setDate(((Comestible) productoSeleccionado).getFechaDeCaducidad());
            } else {
                vista.noComestibleRadioButton.doClick();
                vista.diasGarantiaTxt.setText(String.valueOf((((NoComestible) productoSeleccionado).getDiasDeGarantia())));
            }
        }
    }

    /**
     * Funcion encargada de cerrar la ventana principal
     * @param e Recibe un dato de tipo WindowEvent
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.YES_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

}
