package com.victorarias.practica2;

import com.victorarias.practica2.base.Producto;
import com.victorarias.practica2.gui.ProductosControlador;
import com.victorarias.practica2.gui.ProductosModelo;
import com.victorarias.practica2.gui.Ventana;

/**
 * Created by Victor Arias on 24/11/2020
 * Clase principal de la aplicacion
 * En ella se crea la vista el modelo y con ellos controlador
 */
public class Principal {
    /**
     * Funcion main de la aplicacion
     * @param args parametro de ejecucion de la aplicacion
     */
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        ProductosModelo modelo = new ProductosModelo();
        ProductosControlador controlador = new ProductosControlador(vista, modelo);
    }
}
