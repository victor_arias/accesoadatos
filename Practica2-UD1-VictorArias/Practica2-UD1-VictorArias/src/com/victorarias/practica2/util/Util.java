package com.victorarias.practica2.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Clase encargada de mostrar pestañas con informacion dentro de la app
 */
public class Util {

    /**
     * Funcion que muestra un mensaje de error por pantalla
     * @param mensaje Un String que contiene el mensaje de herror a mostrar
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     *
     * @param mensaje Un String con el mensaje que se va a mostrar al usuario
     * @param titulo Un String con el titulo del mensaje que se va a mostrar
     * @return Devuelve la respuesta del usuario al mensaje de confirmacion
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }

    /**
     * Funcion que se encarga de mostrar una ventana para seleccionar un fichero o indicar como generarlo
     * @param rutaDefecto Un dato tipo File que contiene una ruta para el fichero
     * @param tipoArchivos Un String con el tipo de archivo
     * @param extension Un String con la extension del fichero
     * @return Devuelve un selector de ficheros (JFileChooser)
     */
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivos, String extension) {
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto!=null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}
