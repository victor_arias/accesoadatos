package com.victorarias.practica2.base;

/**
 * Clase que extiende de Producto
 */
public class NoComestible extends Producto {
    private int diasDeGarantia;

    /**
     * Constructor vacio de la clase NoComestible
     */
    public NoComestible() {
    }

    /**
     * Constructor de la clase NoComestible
     * @param nombre Un String con el nombre del Producto NoComestible
     * @param marca Un String con la marca del Producto NoComestible
     * @param precio Un double con el precio del Producto NoComestible
     * @param diasDeGarantia Un int con los dias de garantia de Producto NoComestible
     */
    public NoComestible(String nombre, String marca, double precio, int diasDeGarantia) {
        super(nombre, marca, precio);
        this.diasDeGarantia = diasDeGarantia;
    }

    /**
     * Funcion para obtener los dias de garantia  de un Producto NoComestible
     * @return Devuelve un int con los dias de garantia del Producto NoComestible
     */
    public int getDiasDeGarantia() {
        return diasDeGarantia;
    }

    /**
     * Funcion para establecer los dias de garantia  de un Producto NoComestible
     * @param diasDeGarantia Recibe un int con los dias de garantia del Producto NoComestible
     */
    public void setDiasDeGarantia(int diasDeGarantia) {
        this.diasDeGarantia = diasDeGarantia;
    }

    /**
     * Funcion para obtener un String con los datos del Producto NoComestible
     * @return Un String con los datos del Producto NoComestible
     */
    @Override
    public String toString() {
        return "NoComestible{" +
                "nombre='" + getNombre() + '\'' +
                ", marca='" + getMarca() + '\'' +
                ", precio=" + getPrecio() + '\'' +
                ", diasDeGarantia=" + diasDeGarantia +
                '}';
    }
}
