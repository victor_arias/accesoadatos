package com.victorarias.practica2.base;

import java.time.LocalDate;

/**
 * Clase Comestible extiendo de Producto
 */
public class Comestible extends Producto {
    private LocalDate fechaDeCaducidad;

    /**
     * Constructor vacio de la clase Comestible
     */
    public Comestible() {
    }

    /**
     * Constructor de la clase Comestible
     * @param nombre Un String con el nombre del Producto Comestible
     * @param marca Un String con la marca del Producto Comestible
     * @param precio Un double con el precio del Producto Comestible
     * @param fechaDeCaducidad Un LocalDate con la fecha de caducidad del Producto Comestible
     */
    public Comestible(String nombre, String marca, double precio, LocalDate fechaDeCaducidad) {
        super(nombre, marca, precio);
        this.fechaDeCaducidad = fechaDeCaducidad;
    }

    /**
     * Funcion para obtener la fecha de caducidad de un Producto Comestible
     * @return  Devuelve un LocalDate con la fecha de caducidad del Producto Comestible
     */
    public LocalDate getFechaDeCaducidad() {
        return fechaDeCaducidad;
    }

    /**
     * Funcion para establecer la fecha de caducidad de un Producto Comestible
     * @param fechaDeCaducidad Un dato LocalDate con la fecha de caducidad del nuevo Producto Comestible
     */
    public void setFechaDeCaducidad(LocalDate fechaDeCaducidad) {
        this.fechaDeCaducidad = fechaDeCaducidad;
    }

    /**
     * Funcion para obtener un String con los datos del Producto Comestible
     * @return Un String con los datos del Producto Comestible
     */
    @Override
    public String toString() {
        return "Comestible{" +
                "nombre='" + getNombre() + '\'' +
                ", marca='" + getMarca() + '\'' +
                ", precio=" + getPrecio() + '\'' +
                ", fechaDeCaducidad=" + fechaDeCaducidad +
                '}';
    }
}
