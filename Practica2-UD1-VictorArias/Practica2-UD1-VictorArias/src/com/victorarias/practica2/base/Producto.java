package com.victorarias.practica2.base;

/**
 * Clase padre de Comestible y NoComestible
 */
public abstract class Producto {
    private String nombre;
    private String marca;
    private double precio;

    /**
     * Constructor vacio del Producto
     */
    public Producto() {
    }

    /**
     * Constructor de la clase Producto
     * @param nombre Un String con el nombre del Producto
     * @param marca Un String con la marca del Producto
     * @param precio Un double con el precio del Producto
     */
    public Producto(String nombre, String marca, double precio) {
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
    }

    /**
     * Funcion para obtener el nombre de un Producto
     * @return  Devuelve un String con el nombre del Producto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Funcion para establecer el nombre de un Producto
     * @param nombre Recibe un String con el nombre del Producto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Funcion para obtener la marca de un Producto
     * @return  Devuelve un String con la marca del Producto
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Funcion para establecer la marca de un Producto
     * @param marca Recibe un String con la marca del Producto
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Funcion para obtener el precio de un Producto
     * @return  Devuelve un double con el precio del Producto
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * Funcion para establecer el precio de un Producto
     * @param precio Recibe un double con el precio del Producto
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
