package com.victorarias.practica1.vistas;

import com.victorarias.practica1.clases.Producto;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;


public class Vista {
    private JFrame frame;
    private JPanel panel;
    private JTextField txtNombreProducto;
    private JTextField txtDescripcionProducto;
    private JTextField txtPrecioProducto;
    private JButton btnAltaProducto;
    private JButton btnMostrarProducto;
    private JComboBox comboBox;
    private JTextArea textArea;
    private JTextField txtTipoProducto;
    private JButton btnBorrarProductos;
    private JButton btnBorrarTexto;

    //Elementos añadidos por mi
    private LinkedList<Producto> lista;
    private DefaultComboBoxModel<Producto> dcbm;


    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);

        btnAltaProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //dar de alta producto
                altaProducto(txtNombreProducto.getText(), txtTipoProducto.getText(), txtDescripcionProducto.getText(), txtPrecioProducto.getText());
                //listar productos de la lista del combo
                refrescarComboBox();
            }
        });

        btnMostrarProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // muestro el producto seleccionado
                Producto seleccionado = (Producto) dcbm.getSelectedItem();
                textArea.setText(seleccionado.toString());
            }
        });
        btnBorrarProductos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarProductos();
            }
        });
        btnBorrarTexto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                borrarTextArea();
            }
        });
    }

    /**
     * Añade los productos al comboBox
     */
    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Producto producto : lista) {
            dcbm.addElement(producto);
        }
    }

    /**
     * Elimina lor productos del combo box
     */
    private void vaciarComboBox(){
        dcbm.removeAllElements();
    }

    /**
     * Elimina los productos del programa
     */
    private void eliminarProductos(){
        lista.clear();
        vaciarComboBox();
    }

    /**
     * Borra el texto del textArea
     */
    private void borrarTextArea(){
        textArea.setText("");
    }

    private void altaProducto(String nombre, String tipo, String descripcion, String precio) {
        lista.add(new Producto(nombre, tipo, descripcion, Float.parseFloat(precio)));
    }

    /**
     * Creamos el menu donde se muestran las opciones de importar y exportar xml
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }

            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    /**
     * Permite importar los productos desde un fichero .xml
     * @param fichero Recibe un fichero
     */
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos producto para obtener sus campos
            NodeList productos = documento.getElementsByTagName("producto");
            eliminarProductos();
            for (int i = 0; i < productos.getLength(); i++) {
                Node producto = productos.item(i);
                Element elemento = (Element) producto;

                //Obtengo los campos nombre tipo descripcion y precio
                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String tipo = elemento.getElementsByTagName("tipo").item(0).getChildNodes().item(0).getNodeValue();
                String descripcion = elemento.getElementsByTagName("descripcion").item(0).getChildNodes().item(0).getNodeValue();
                String precio = elemento.getElementsByTagName("precio").item(0).getChildNodes().item(0).getNodeValue();

                altaProducto(nombre, tipo, descripcion, precio);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permite exportar los productos a .xml
     * @param fichero recibe un fichero
     */
    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (productos) y lo añado al documento
            Element raiz = documento.createElement("productos");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoProducto;
            Element nodoDatos;
            Text dato;

            //Por cada producto de la lista, creo un nodo producto
            for (Producto producto : lista) {

                //Creo un nodo producto y lo añado al nodo raiz (productos)
                nodoProducto = documento.createElement("producto");
                raiz.appendChild(nodoProducto);

                //A cada nodo producto le añado los nodos nombre tipo descripcion y precios
                nodoDatos = documento.createElement("nombre");
                nodoProducto.appendChild(nodoDatos);

                dato = documento.createTextNode(producto.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("tipo");
                nodoProducto.appendChild(nodoDatos);

                dato = documento.createTextNode(producto.getTipo());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("descripcion");
                nodoProducto.appendChild(nodoDatos);

                dato = documento.createTextNode(producto.getDescripcion());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("precio");
                nodoProducto.appendChild(nodoDatos);

                dato = documento.createTextNode(String.valueOf(producto.getPrecio()));
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
