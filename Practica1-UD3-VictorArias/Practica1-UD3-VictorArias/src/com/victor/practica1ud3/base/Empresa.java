package com.victor.practica1ud3.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "empresas", schema = "supermercado", catalog = "")
public class Empresa {
    private int id;
    private String nombre;
    private String email;
    private String telefono;
    private String tipoempresa;
    private String web;
    private List<Producto> productos;
    private List<Marca> marcas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "tipoempresa")
    public String getTipoempresa() {
        return tipoempresa;
    }

    public void setTipoempresa(String tipoempresa) {
        this.tipoempresa = tipoempresa;
    }

    @Basic
    @Column(name = "web")
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empresa empresa = (Empresa) o;
        return id == empresa.id &&
                Objects.equals(nombre, empresa.nombre) &&
                Objects.equals(email, empresa.email) &&
                Objects.equals(telefono, empresa.telefono) &&
                Objects.equals(tipoempresa, empresa.tipoempresa) &&
                Objects.equals(web, empresa.web);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, email, telefono, tipoempresa, web);
    }

    @OneToMany(mappedBy = "empresa")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @ManyToMany(mappedBy = "empresas")
    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }
}
