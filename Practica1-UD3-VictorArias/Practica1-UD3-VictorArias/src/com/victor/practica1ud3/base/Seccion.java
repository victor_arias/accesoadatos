package com.victor.practica1ud3.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "secciones", schema = "supermercado", catalog = "")
public class Seccion {
    private int id;
    private String nombre;
    private String tamanyo;
    private boolean habierta;
    private List<SeccionProducto> seccionproducto;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "tamanyo")
    public String getTamanyo() {
        return tamanyo;
    }

    public void setTamanyo(String tamanyo) {
        this.tamanyo = tamanyo;
    }

    @Basic
    @Column(name = "habierta")
    public boolean isHabierta() {
        return habierta;
    }

    public void setHabierta(boolean habierta) {
        this.habierta = habierta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seccion seccion = (Seccion) o;
        return id == seccion.id &&
                habierta == seccion.habierta &&
                Objects.equals(nombre, seccion.nombre) &&
                Objects.equals(tamanyo, seccion.tamanyo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, tamanyo, habierta);
    }

    @OneToMany(mappedBy = "seccion")
    public List<SeccionProducto> getSeccionproducto() {
        return seccionproducto;
    }

    public void setSeccionproducto(List<SeccionProducto> seccionproducto) {
        this.seccionproducto = seccionproducto;
    }
}
