package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

/**
 * Autor: Victor Arias Benito
 */


/**
 * Metodo principal de la aplicacion
 */
public class Principal {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
