package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboSeccion de la vista.
 * Representan los géneros literarios que existen.
 */
public enum SeccionesProductos {
    FRUTAS("Frutas"),
    LACTEOS("Lacteos"),
    CARNES("Carnes"),
    PESCADOS("Pescados");

    private String valor;

    SeccionesProductos(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
