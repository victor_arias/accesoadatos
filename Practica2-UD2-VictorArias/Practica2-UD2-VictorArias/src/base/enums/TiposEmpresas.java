package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboTipoEmpresa de la vista.
 * Representan los tipos de empresa que existen.
 */
public enum TiposEmpresas {
    SOCIEDADLIMITADA("Sociedad Limitada"),
    SOCIEDADCIVIL("Sociedad Civil"),
    EMPRESARIOINDIVIDUAL("Empresario invdividual"),
    SOCIEDADCOLECTIVA("Sociedad Colectiva");

    private String valor;

    TiposEmpresas(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }

}
