package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;


public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;


    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Carga los datos desde un fichero
     */
    private Connection conexion;

    /**
     * Funcion que se conecta a la base de datos supermercado
     */
    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/supermercado",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Funcion que devuelve un String con el texto de basedatos_java.sql
     * @return String
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Funcion que desconecta de la base de datos
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     * Inserta una marca en la tabla de marcas
     * @param nombre
     * @param origen
     * @param fechaCreacion
     * @param pais
     */
    void insertarMarca(String nombre, String origen, LocalDate fechaCreacion, String pais) {
        String sentenciaSql = "INSERT INTO marcas (nombre, origen, fechacreacion, pais) VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, origen);
            sentencia.setDate(3, Date.valueOf(fechaCreacion));
            sentencia.setString(4, pais);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Inserta una empresa en la tabla de empresas
     * @param nombre
     * @param email
     * @param telefono
     * @param tipoEmpresa
     * @param web
     */
    void insertarEmpresa(String nombre, String email, String telefono, String tipoEmpresa, String web) {
        String sentenciaSql = "INSERT INTO empresas (nombre, email, telefono, tipoempresa, web) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEmpresa);
            sentencia.setString(5, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Inserta un producto en la tabla productos
     * @param nombre
     * @param codigo
     * @param empresa
     * @param seccion
     * @param marca
     * @param precio
     * @param fechaCaducidad
     */
    void insertarProducto(String nombre, String codigo, String empresa, String seccion, String marca,
                       float precio, LocalDate fechaCaducidad) {
        String sentenciaSql = "INSERT INTO productos (nombre, codigo, idempresa, seccion, idmarca, precio, fechacaducidad) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idEmpresa = Integer.valueOf(empresa.split(" ")[0]);
        int idMarca = Integer.valueOf(marca.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, codigo);
            sentencia.setInt(3, idEmpresa);
            sentencia.setString(4, seccion);
            sentencia.setInt(5, idMarca);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechaCaducidad));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modifica la empresa con idEmpresa
     * @param nombre
     * @param email
     * @param telefono
     * @param tipoEmpresa
     * @param web
     * @param idEmpresa
     */
    void modificarEmpresa(String nombre, String email, String telefono, String tipoEmpresa, String web, int idEmpresa) {

        String sentenciaSql = "UPDATE empresas SET nombre = ?, email = ?, telefono = ?, tipoempresa = ?, web = ?" +
                "WHERE idempresa = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEmpresa);
            sentencia.setString(5, web);
            sentencia.setInt(6, idEmpresa);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modifica la marca con idMarca
     * @param nombre
     * @param origen
     * @param fechaCreacion
     * @param pais
     * @param idMarca
     */
    void modificarMarca(String nombre, String origen, LocalDate fechaCreacion, String pais, int idMarca) {

        String sentenciaSql = "UPDATE marcas SET nombre = ?, origen = ?, fechacreacion = ?, pais = ?" +
                "WHERE idmarca = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, origen);
            sentencia.setDate(3, Date.valueOf(fechaCreacion));
            sentencia.setString(4, pais);
            sentencia.setInt(5, idMarca);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modifica el producto con idProducto
     * @param nombre
     * @param codigo
     * @param empresa
     * @param seccion
     * @param marca
     * @param precio
     * @param fechaCaducidad
     * @param idProducto
     */
    void modificarProducto(String nombre, String codigo, String empresa, String seccion, String marca,
                           float precio, LocalDate fechaCaducidad, int idProducto) {

        String sentenciaSql = "UPDATE productos SET nombre = ?, codigo = ?, idempresa = ?, codigo = ?, " +
                "idmarca = ?, precio = ?, fechacaducidad = ? WHERE idproducto = ?";
        PreparedStatement sentencia = null;

        int idEmpresa = Integer.valueOf(empresa.split(" ")[0]);
        int idMarca = Integer.valueOf(marca.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, codigo);
            sentencia.setInt(3, idEmpresa);
            sentencia.setString(4, seccion);
            sentencia.setInt(5, idMarca);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechaCaducidad));
            sentencia.setInt(8, idProducto);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     * Este metodo elimina una empresa
     * @param idEmpresa id de la empresa
     */
    void borrarEmpresa(int idEmpresa) {
        String sentenciaSql = "DELETE FROM empresas WHERE idempresa = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idEmpresa);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     * Este metodo elimina una marca
     * @param idMarca id del marca
     */
    void borrarMarca(int idMarca) {
        String sentenciaSql = "DELETE FROM marcas WHERE idmarca = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idMarca);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo elimina un producto
     * @param idProducto id del producto
     */
    void borrarProducto(int idProducto) {
        String sentenciaSql = "DELETE FROM productos WHERE idproducto = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idProducto);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo devuelve las empresas guardadas en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarEmpresa() throws SQLException {
        String sentenciaSql = "SELECT concat(idempresa) as 'ID', concat(nombre) as 'Nombre empresa', concat(email) as 'Email', " +
                "concat(telefono) as 'Teléfono', concat(tipoempresa) as 'Tipo', concat(web) as 'Web' FROM empresas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    /**
     * Este metodo devuelve las marcas guardadas en la bbdd
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarMarca() throws SQLException {
        String sentenciaSql = "SELECT concat(idmarca) as 'ID', concat(nombre) as 'Nombre', concat(origen) as 'Origen', " +
                "concat(fechacreacion) as 'Fecha de creacion', concat(pais) as 'País de origen' FROM marcas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve todos los productos de la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarProductos() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idproducto) as 'ID', concat(b.nombre) as 'Nombre', concat(b.codigo) as 'Codigo', " +
                "concat(e.idempresa, ' - ', e.nombre) as 'Empresa', concat(b.seccion) as 'Seccion', " +
                "concat(a.idmarca, ' - ', a.origen, ', ', a.nombre) as 'Marca', " +
                "concat(b.precio) as 'Precio', concat(b.fechacaducidad) as 'Fecha de caducidad'" +
                " FROM productos as b " +
                "inner join empresas as e on e.idempresa = b.idempresa inner join " +
                "marcas as a on a.idmarca = b.idmarca";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }


    /**
     * Este metodo comprueba si el codigo de un producto ya esta en la base de datos
     * @param codigo codigo del producto
     * @return true si ya existe
     */
    public boolean productoCodigoYaExiste(String codigo) {
        String salesConsult = "SELECT existeCodigo(?)";
        PreparedStatement function;
        boolean codigoExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, codigo);
            ResultSet rs = function.executeQuery();
            rs.next();

            codigoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoExists;
    }

    /**
     * Este metodo comprueba si el nombre de la empresa ya existe en la base de datos
     * @param nombre nombre de la empresa
     * @return true si ya existe
     */
    public boolean empresaNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeNombreEmpresa(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Este metodo comprueba si el nombre completo de una marca
     * ya existe en la base de datos
     * @param nombre nombre de la marca
     * @param origen origen de la marca
     * @return true si existe
     */
    public boolean marcaNombreYaExiste(String nombre, String origen) {
        String completeName = origen + ", " + nombre;
        String marcaNameConsult = "SELECT existeNombreMarca(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(marcaNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }



}
