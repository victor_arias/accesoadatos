package gui;

import base.enums.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{

    private final static String TITULOFRAME = "Practica2Ud2";
    private JPanel panel1;
    private JTabbedPane tabbedPane;
    /*MARCA*/
    JTextField txtNombreM;
    JTextField txtOrigenM;
    JTextField txtPaisM;
    JTable marcasTabla;
    JButton btnMarcasAnadir;
    JButton btnMarcasModificar;
    JButton btnMarcasEliminar;
    DatePicker fechaCreacionM;
    /*EMPRESA*/
    JTextField txtNombreE;
    JTextField txtEmailE;
    JTextField txtTelefonoE;
    JTable empresasTabla;
    JButton btnEmpresasEliminar;
    JButton btnEmpresasAnadir;
    JButton btnEmpresasModificar;
    JComboBox<String> comboTipoEmpresaE;
    JTextField txtWebE;
    /*PRODUCTO*/
    JTextField txtNombreP;
    JComboBox<String> comboMarcaP;
    JComboBox<String> comboEmpresaP;
    JComboBox<String> comboSeccionP;
    JTextField txtCodigoP;
    JTextField txtPrecioProductoP;
    JTable productosTabla;
    JButton btnProductosEliminar;
    JButton btnProductosAnadir;
    JButton btnProductosModificar;
    DatePicker fechaP;
    /*SEARCH*/
    JLabel etiquetaEstado;
    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmEmpresas;
    DefaultTableModel dtmMarcas;
    DefaultTableModel dtmProductos;
    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {

        this.dtmProductos = new DefaultTableModel();
        this.productosTabla.setModel(dtmProductos);

        this.dtmMarcas = new DefaultTableModel();
        this.marcasTabla.setModel(dtmMarcas);

        this.dtmEmpresas = new DefaultTableModel();
        this.empresasTabla.setModel(dtmEmpresas);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TiposEmpresas constant : TiposEmpresas.values()) { comboTipoEmpresaE.addItem(constant.getValor()); }
        comboTipoEmpresaE.setSelectedIndex(-1);

        for(SeccionesProductos constant : SeccionesProductos.values()) { comboSeccionP.addItem(constant.getValor()); }
        comboSeccionP.setSelectedIndex(-1);
    }

    /**
     * Setea un JDialog para introducir la contreña de administrador y poder configurar la base de datos
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}
