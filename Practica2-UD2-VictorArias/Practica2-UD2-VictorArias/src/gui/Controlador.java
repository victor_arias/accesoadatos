package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos
     * desde un fichero, añade los listeners y lista los datos.
     *
     * @param modelo Modelo
     * @param vista  Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * Refresca las secciones de la app añadiendo los datos que falten
     */
    private void refrescarTodo() {
        refrescarMarcas();
        refrescarEmpresas();
        refrescarProductos();
        refrescar = false;
    }

    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnProductosAnadir.addActionListener(listener);
        vista.btnMarcasAnadir.addActionListener(listener);
        vista.btnEmpresasAnadir.addActionListener(listener);
        vista.btnProductosEliminar.addActionListener(listener);
        vista.btnMarcasEliminar.addActionListener(listener);
        vista.btnEmpresasEliminar.addActionListener(listener);
        vista.btnProductosModificar.addActionListener(listener);
        vista.btnMarcasModificar.addActionListener(listener);
        vista.btnEmpresasModificar.addActionListener(listener);

        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.empresasTabla.getSelectionModel())) {
                int row = vista.empresasTabla.getSelectedRow();
                vista.txtNombreE.setText(String.valueOf(vista.empresasTabla.getValueAt(row, 1)));
                vista.txtEmailE.setText(String.valueOf(vista.empresasTabla.getValueAt(row, 2)));
                vista.txtTelefonoE.setText(String.valueOf(vista.empresasTabla.getValueAt(row, 3)));
                vista.txtWebE.setText(String.valueOf(vista.empresasTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.marcasTabla.getSelectionModel())) {
                int row = vista.marcasTabla.getSelectedRow();
                vista.txtNombreM.setText(String.valueOf(vista.marcasTabla.getValueAt(row, 1)));
                vista.txtOrigenM.setText(String.valueOf(vista.marcasTabla.getValueAt(row, 2)));
                vista.fechaCreacionM.setDate((Date.valueOf(String.valueOf(vista.marcasTabla.getValueAt(row, 3)))).toLocalDate());
                vista.txtPaisM.setText(String.valueOf(vista.marcasTabla.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.productosTabla.getSelectionModel())) {
                int row = vista.productosTabla.getSelectedRow();
                vista.txtNombreP.setText(String.valueOf(vista.productosTabla.getValueAt(row, 1)));
                vista.comboMarcaP.setSelectedItem(String.valueOf(vista.productosTabla.getValueAt(row, 5)));
                vista.comboEmpresaP.setSelectedItem(String.valueOf(vista.productosTabla.getValueAt(row, 3)));
                vista.comboSeccionP.setSelectedItem(String.valueOf(vista.productosTabla.getValueAt(row, 4)));
                vista.fechaP.setDate((Date.valueOf(String.valueOf(vista.productosTabla.getValueAt(row, 7)))).toLocalDate());
                vista.txtCodigoP.setText(String.valueOf(vista.productosTabla.getValueAt(row, 2)));
                vista.txtPrecioProductoP.setText(String.valueOf(vista.productosTabla.getValueAt(row, 6)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.empresasTabla.getSelectionModel())) {
                    borrarCamposEmpresas();
                } else if (e.getSource().equals(vista.marcasTabla.getSelectionModel())) {
                    borrarCamposMarcas();
                } else if (e.getSource().equals(vista.productosTabla.getSelectionModel())) {
                    borrarCamposProductos();
                }
            }
        }
    }


    /**
     * Añade sus respectivas acciones a los botones de añadir, modificar y eliinar de las listas.
     * Añade sus respectivas acciones a los menus de guardar, guardar como y mostrar opciones.
     * Añade la acción de guardar las opciones al dialog de opciones.
     * Añade las acciones de guardar, no guardar y cancelar a los botones del dialog de guardar
     *  cambios cuando se intenta salir de la aplicación una vez se ha desactivado el autoguardado.
     * @param e Evento producido al pulsar un botón o menu
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "anadirProducto": {
                try {
                    if (comprobarProductoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.productosTabla.clearSelection();
                    } else if (modelo.productoCodigoYaExiste(vista.txtCodigoP.getText())) {
                        Util.showErrorAlert("Ese Codigo de producto ya existe.\nIntroduce un producto diferente");
                        vista.productosTabla.clearSelection();
                    } else {
                        modelo.insertarProducto(
                                vista.txtNombreP.getText(),
                                vista.txtCodigoP.getText(),
                                String.valueOf(vista.comboEmpresaP.getSelectedItem()),
                                String.valueOf(vista.comboSeccionP.getSelectedItem()),
                                String.valueOf(vista.comboMarcaP.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioProductoP.getText()),
                                vista.fechaP.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.productosTabla.clearSelection();
                }
                borrarCamposProductos();
                refrescarProductos();
            }
            break;
            case "modificarProducto": {
                try {
                    if (comprobarProductoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.productosTabla.clearSelection();
                    } else {
                        modelo.modificarProducto(
                                vista.txtNombreP.getText(),
                                vista.txtCodigoP.getText(),
                                String.valueOf(vista.comboEmpresaP.getSelectedItem()),
                                String.valueOf(vista.comboSeccionP.getSelectedItem()),
                                String.valueOf(vista.comboMarcaP.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioProductoP.getText()),
                                vista.fechaP.getDate(),
                                Integer.parseInt((String)vista.productosTabla.getValueAt(vista.productosTabla.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.productosTabla.clearSelection();
                }
                borrarCamposProductos();
                refrescarProductos();
            }
            break;
            case "eliminarProducto":
                modelo.borrarProducto(Integer.parseInt((String)vista.productosTabla.getValueAt(vista.productosTabla.getSelectedRow(), 0)));
                borrarCamposProductos();
                refrescarProductos();
                break;
            case "anadirMarca": {
                try {
                    if (comprobarMarcaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.marcasTabla.clearSelection();
                    } else if (modelo.marcaNombreYaExiste(vista.txtNombreM.getText(),
                            vista.txtOrigenM.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una marca diferente");
                        vista.marcasTabla.clearSelection();
                    } else {
                        modelo.insertarMarca(vista.txtNombreM.getText(),
                                vista.txtOrigenM.getText(),
                                vista.fechaCreacionM.getDate(),
                                vista.txtPaisM.getText());
                        refrescarMarcas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.marcasTabla.clearSelection();
                }
                borrarCamposMarcas();
            }
            break;
            case "modificarMarca": {
                try {
                    if (comprobarMarcaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.marcasTabla.clearSelection();
                    } else {
                        modelo.modificarMarca(vista.txtNombreM.getText(), vista.txtOrigenM.getText(),
                                vista.fechaCreacionM.getDate(), vista.txtPaisM.getText(),
                                Integer.parseInt((String)vista.marcasTabla.getValueAt(vista.marcasTabla.getSelectedRow(), 0)));
                        refrescarMarcas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.marcasTabla.clearSelection();
                }
                borrarCamposMarcas();
            }
            break;
            case "eliminarMarca":
                modelo.borrarMarca(Integer.parseInt((String)vista.marcasTabla.getValueAt(vista.marcasTabla.getSelectedRow(), 0)));
                borrarCamposMarcas();
                refrescarMarcas();
                break;
            case "anadirEmpresa": {
                try {
                    if (comprobarEmpresaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empresasTabla.clearSelection();
                    } else if (modelo.empresaNombreYaExiste(vista.txtNombreE.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una empresa diferente.");
                        vista.empresasTabla.clearSelection();
                    } else {
                        modelo.insertarEmpresa(vista.txtNombreE.getText(), vista.txtEmailE.getText(),
                                vista.txtTelefonoE.getText(),
                                (String) vista.comboTipoEmpresaE.getSelectedItem(),
                                vista.txtWebE.getText());
                        refrescarEmpresas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.empresasTabla.clearSelection();
                }
                borrarCamposEmpresas();
            }
            break;
            case "modificarEmpresa": {
                try {
                    if (comprobarEmpresaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empresasTabla.clearSelection();
                    } else {
                        modelo.modificarEmpresa(vista.txtNombreE.getText(), vista.txtEmailE.getText(), vista.txtTelefonoE.getText(),
                                String.valueOf(vista.comboTipoEmpresaE.getSelectedItem()), vista.txtWebE.getText(),
                                Integer.parseInt((String)vista.empresasTabla.getValueAt(vista.empresasTabla.getSelectedRow(), 0)));
                        refrescarEmpresas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.empresasTabla.clearSelection();
                }
                borrarCamposEmpresas();
            }
            break;
            case "eliminarEmpresa":
                modelo.borrarEmpresa(Integer.parseInt((String)vista.empresasTabla.getValueAt(vista.empresasTabla.getSelectedRow(), 0)));
                borrarCamposEmpresas();
                refrescarEmpresas();
                break;
        }
    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará. Si no lo están, aparecerá una ventana
     * que permitirá al usuario decidir que hacer con esos cambios no guardados o bien cancelar el cierre
     * de la aplicación.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    /**
     * Actualiza las empresa que se ven en la lista y los comboboxes
     */
    private void refrescarEmpresas() {
        try {
            vista.empresasTabla.setModel(construirTableModelEmpresas(modelo.consultarEmpresa()));
            vista.comboEmpresaP.removeAllItems();
            for(int i = 0; i < vista.dtmEmpresas.getRowCount(); i++) {
                vista.comboEmpresaP.addItem(vista.dtmEmpresas.getValueAt(i, 0)+" - "+
                        vista.dtmEmpresas.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Funcion que devuelve un tambleModel con los datos
     * @param rs
     * @return
     * @throws SQLException
     */
    private  DefaultTableModel construirTableModelEmpresas(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmEmpresas.setDataVector(data, columnNames);

        return vista.dtmEmpresas;

    }
    /**
     * Actualiza las marcas que se ven en la lista y los comboboxes
     */
    private void refrescarMarcas() {
        try {
            vista.marcasTabla.setModel(construirTableModeloMarcas(modelo.consultarMarca()));
            vista.comboMarcaP.removeAllItems();
            for(int i = 0; i < vista.dtmMarcas.getRowCount(); i++) {
                vista.comboMarcaP.addItem(vista.dtmMarcas.getValueAt(i, 0)+" - "+
                        vista.dtmMarcas.getValueAt(i, 2)+", "+vista.dtmMarcas.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Funcion que devuelve un tambleModel con los datos
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModeloMarcas(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmMarcas.setDataVector(data, columnNames);

        return vista.dtmMarcas;

    }

    /**
     * Actualiza los productos que se ven en la lista y los comboboxes
     */
    private void refrescarProductos() {
        try {
            vista.productosTabla.setModel(construirTableModelProductos(modelo.consultarProductos()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Funcion que devuelve un tambleModel con los datos
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelProductos(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmProductos.setDataVector(data, columnNames);

        return vista.dtmProductos;
    }

    /**
     * Añade al Vector data los datos del ResultSet rs
     * @param rs
     * @param columnCount
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Vacía los campos de la tab de productos
     */
    private void borrarCamposProductos() {
        vista.comboEmpresaP.setSelectedIndex(-1);
        vista.comboMarcaP.setSelectedIndex(-1);
        vista.txtNombreP.setText("");
        vista.txtCodigoP.setText("");
        vista.comboSeccionP.setSelectedIndex(-1);
        vista.txtPrecioProductoP.setText("");
        vista.fechaP.setText("");
    }

    /**
     * Vacía los campos de la tab de marcas
     */
    private void borrarCamposMarcas() {
        vista.txtNombreM.setText("");
        vista.txtOrigenM.setText("");
        vista.txtPaisM.setText("");
        vista.fechaCreacionM.setText("");
    }

    /**
     * Vacía los campos de la tab de empresas
     */
    private void borrarCamposEmpresas() {
        vista.txtNombreE.setText("");
        vista.txtEmailE.setText("");
        vista.txtTelefonoE.setText("");
        vista.comboTipoEmpresaE.setSelectedIndex(-1);
        vista.txtWebE.setText("");
    }

    /**
     * Comprueba que los campos necesarios para añadir un producto estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarProductoVacio() {
        return vista.txtNombreP.getText().isEmpty() ||
                vista.txtPrecioProductoP.getText().isEmpty() ||
                vista.txtCodigoP.getText().isEmpty() ||
                vista.comboSeccionP.getSelectedIndex() == -1 ||
                vista.comboMarcaP.getSelectedIndex() == -1 ||
                vista.comboEmpresaP.getSelectedIndex() == -1 ||
                vista.fechaP.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una marca estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarMarcaVacia() {
        return vista.txtOrigenM.getText().isEmpty() ||
                vista.txtNombreM.getText().isEmpty() ||
                vista.txtPaisM.getText().isEmpty() ||
                vista.fechaCreacionM.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una empresa estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarEmpresaVacia() {
        return vista.txtNombreE.getText().isEmpty() ||
                vista.txtEmailE.getText().isEmpty() ||
                vista.txtTelefonoE.getText().isEmpty() ||
                vista.comboTipoEmpresaE.getSelectedIndex() == -1 ||
                vista.txtWebE.getText().isEmpty();
    }

    /*LISTENERS IPLEMENTOS NO UTILIZADOS*/

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }


}
