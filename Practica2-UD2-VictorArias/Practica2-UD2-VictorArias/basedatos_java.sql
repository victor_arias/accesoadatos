CREATE DATABASE if not exists supermercado;
--
USE supermercado;
--
create table if not exists marcas (
                                       idmarca int auto_increment primary key,
                                       nombre varchar(50) not null,
                                       origen varchar(150) not null,
                                       fechacreacion date,
                                       pais varchar(50));
--
create table if not exists empresas (
                                       idempresa int auto_increment primary key,
                                       nombre varchar(50) not null,
                                       email varchar(100) not null,
                                       telefono varchar(9),
                                       tipoempresa varchar(50),
                                       web varchar(500));
--
create table if not exists productos(
                                     idproducto int auto_increment primary key,
                                     idmarca int not null,
                                     idempresa int not null,
                                     nombre varchar(50) not null,
                                     codigo varchar(40) not null UNIQUE,
                                     seccion varchar(30),
                                     precio float not null,
                                     fechacaducidad date);
--
alter table productos
    add foreign key (idmarca) references marcas(idmarca),
    add foreign key (idempresa) references empresas(idempresa);
--
create function existeCodigo(f_codigo varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idproducto) from productos)) do
            if  ((select codigo from productos where idproducto = (i + 1)) like f_codigo) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;
--
create function existeNombreEmpresa(f_name varchar(50))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idempresa) from empresas)) do
            if  ((select nombre from empresas where idempresa = (i + 1)) like f_name) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;
--
create function existeNombreMarca(f_name varchar(202))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idmarca) from marcas)) do
            if  ((select concat(origen, ', ', nombre) from marcas where idmarca = (i + 1)) like f_name) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;
